<?php require_once(dirname(__FILE__) . "/config/config.php"); ?>

<?php $page_name = 'Admin Login'; ?>

<?php include(dirname(__FILE__) . '/template/header.php'); ?>

<?php include(dirname(__FILE__) . '/template/topbar.php'); ?>

<?php

if (array_key_exists("login", $_GET)) {
	$oauth_provider = $_GET['oauth_provider'];
	if ($oauth_provider == 'twitter') {
		header("Location: ".SITE_URL."login-twitter/");
	} 
} 
?>

<div class="account-container">
    <div class="content clearfix">
        <form action="<?php echo SITE_URL; ?>category/" method="post">
            <h1>Member Login</h1>		
            <div class="login-fields">
                <p>Please provide your details</p>
                <div class="field">
                    <label for="username">Username</label>
                    <input type="text" id="username" name="username" value="" placeholder="Username" class="login username-field" required="required" />
                </div>
                <div class="field">
                    <label for="password">Password:</label>
                    <input type="password" id="password" name="password" value="" placeholder="Password" class="login password-field" required="required" />
                </div>
            </div>
            <div class="login-actions">
                <span class="login-checkbox">
                    <input id="Field" name="Field" type="checkbox" class="field login-checkbox" value="First Choice" tabindex="4" />
                    <label class="choice" for="Field">Keep me signed in</label>
                </span>
                <button class="button btn btn-success btn-large">Sign In</button>
            </div>
        </form>
		<div id="buttons">
        	<center><span class="text-warning">OR</span></div>
            <a href="<?php echo SITE_URL; ?>?login&oauth_provider=twitter"><img src="<?php echo SITE_URL; ?>img/sign_in.png" width="100%" /></a>&nbsp;&nbsp;&nbsp;
        </div>
    </div>
</div>
<div class="login-extra">
    <a href="#">Reset Password</a>
</div>
    
<?php include(dirname(__FILE__) . '/template/footer.php'); ?>