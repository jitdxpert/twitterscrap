<?php 
require_once(dirname(__FILE__) . "/config/config.php");
require_once(dirname(__FILE__) . "/config/mongodb.php");

$msg = '';
if ( isset($_POST['save']) ) {
	$screen_name_array = array();
	$category = addslashes($_POST['category']);
	$catSQL = mysql_query("INSERT INTO `twscrapp_category`(`twscrapp_category_name`) VALUES ('$category')");
	$category_id = mysql_insert_id();
	$influencers = $_POST['influencer'];
	foreach ( $influencers as $influencer ) {
		$screen_name_array[] = $influencer;
	}
	$twitteroauth = new TwitterOAuth(YOUR_CONSUMER_KEY, YOUR_CONSUMER_SECRET, $_SESSION['access_token']['oauth_token'], $_SESSION['access_token']['oauth_token_secret']);
	foreach ( $screen_name_array as $screen_name ) {
		$user_details 	= $twitteroauth->get('users/show', array("screen_name" => urlencode($screen_name)));
		$influence_sql 	= "SELECT * FROM `twscrapp_influence` WHERE `influncer_twitter_id` = '" . $user_details->id . "'";
		$infuence_sql_result = mysql_query($influence_sql);
		if ( $infuence_sql_result ) {
			$infuence_sql_count = mysql_num_rows($infuence_sql_result);
		} else {
			$infuence_sql_count = 0;
		}	
		if ( $infuence_sql_count > 0 ) {
			$influence_result_data = mysql_fetch_assoc($infuence_sql_result);
			$sql_influncer_update = "UPDATE `twscrapp_influence` SET `influncer_twitter_screenname` = '" . $user_details->screen_name . "', `influncer_twitter_bio` = '" . addslashes($user_details->description) . "', `influencer_website` = '" . $user_details->url . "', `influencer_profile_pic` = '" . $user_details->profile_image_url . "' WHERE `influncer_twitter_id` = '" . $user_details->id . "'";
			$sql_influncer_update_result = mysql_query($sql_influncer_update);
			$cursor = $influence_result_data['influncer_cursor_id'];
			$followers = $twitteroauth->get('followers/list', array('screen_name' => $user_details->screen_name, 'cursor' => $cursor));
			foreach ( $followers->users as $follower ) {
				$insertData = array(
					"id_str"		=> $follower->id_str, 
					"name"			=> $follower->name, 
					"screen_name"	=> $follower->screen_name, 
					"location"		=> $follower->location, 
					"description"	=> $follower->description, 
					"website"		=> $follower->url, 
					"profile_pic"	=> $follower->profile_image_url, 
					"influncer_id"	=> $user_details->id_str, 
				);
				$mongoDb->influence_followers->insert($insertData);
			}
			
			$sql_influncer_update = "UPDATE `twscrapp_influence` SET `influncer_cursor_id` = '" . $followers->next_cursor_str . "' WHERE `influncer_twitter_id` = '" . $user_details->id . "'";
			$sql_influncer_update_result = mysql_query($sql_influncer_update);
			$cursor = '';
			$msg = 'A New Category Successfully Added!';
		} else {
			$sql_influncer_insert = "INSERT INTO `twscrapp_influence`(`influence_category_id`, `influncer_twitter_id`, `influncer_twitter_screenname`, `influncer_twitter_bio`, `influencer_website`, `influencer_profile_pic`) VALUES($category_id, '" . $user_details->id . "', '" . $user_details->screen_name . "', '" . addslashes($user_details->description) . "', '" . $user_details->url . "', '" . $user_details->profile_image_url . "')";
			$sql_influncer_result = mysql_query($sql_influncer_insert);
			$cursor = -1;
			$followers = $twitteroauth->get('followers/list', array('screen_name' => $user_details->screen_name, 'cursor' => $cursor));
			foreach ( $followers->users as $follower ) {
				$insertData = array(
					"id_str"		=> $follower->id_str, 
					"name"			=> $follower->name, 
					"screen_name"	=> $follower->screen_name, 
					"location"		=> $follower->location, 
					"description"	=> $follower->description, 
					"website"		=> $follower->url, 
					"profile_pic"	=> $follower->profile_image_url, 
					"influncer_id"	=> $user_details->id_str, 
				);
				$mongoDb->influence_followers->insert($insertData);
			}
			
			$sql_influncer_update = "UPDATE `twscrapp_influence` SET `influncer_cursor_id` = '" . $followers->next_cursor_str . "' WHERE `influncer_twitter_id` = '" . $user_details->id . "'";
			$sql_influncer_update_result = mysql_query($sql_influncer_update);
			$cursor = '';
			$msg = 'A New Category Successfully Added!';
		}
	}
}
?>

<?php $page_name = 'category'; ?>

<?php include(dirname(__FILE__) . '/template/header.php'); ?>

<?php include(dirname(__FILE__) . '/template/topbar.php'); ?>
    
<?php include(dirname(__FILE__) . '/template/navbar.php'); ?>

<div class="main">
	<div class="main-inner">
    	<div class="container">
        	<div class="row">
            	<div class="span6">
                	<div class="widget widget-table action-table">
                        <div class="widget-header">
                            <i class="icon-th-list"></i>
                            <h3>Categories</h3>
                        </div>
                        <div class="widget-content">
                        	<?php 
							$SelectCatSQL = mysql_query("SELECT * FROM `twscrapp_category` ORDER BY `twscrapp_category_id` DESC");
							if ( $SelectCatSQL ) {
								$SelectCatRow = mysql_num_rows($SelectCatSQL);
							} else {
								$SelectCatRow = 0;
							} ?>
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Category ID</th>
                                        <th>Category Name</th>
                                        <th class="td-actions"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php if ( $SelectCatRow > 0 ) { ?>
										<?php while ( $CatData = mysql_fetch_assoc($SelectCatSQL) ) { ?>
                                            <tr>
                                                <td><?php echo $CatData['twscrapp_category_id']; ?></td>
                                                <td><?php echo $CatData['twscrapp_category_name']; ?></td>
                                                <td class="td-actions">
                                                    <a href="#" class="btn btn-small btn-success">
                                                        <i class="btn-icon-only icon-ok"> </i>
                                                    </a>
                                                    <a href="#" class="btn btn-danger btn-small">
                                                        <i class="btn-icon-only icon-remove"> </i>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    <?php } else { ?>
                                    	<tr>
                                            <td colspan="3">No categories are available.</td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
            	<div class="span6">
                	<div class="widget">
                    	<div class="widget-header">
                        	<i class="icon-list-alt"></i>
                            <h3>Create Category</h3>
                        </div>
                        <div class="widget-content">
                        	<?php if ( $msg ) { ?><div class="alert alert-success"><?php echo $msg; ?></div><?php } ?>
							<form action="" method="post" class="form-horizontal">
                            	<fieldset>
                                  	<div class="control-group">
                                    	<label class="control-label" for="category">Category Name: </label>
                                        <div class="controls">
                                        	<input type="text" class="span4" id="category" name="category" />
                                        </div>
                                    </div>
                                    <div class="control-group">
                                    	<label class="control-label" for="influencer">Influencer Name: </label>
                                        <div class="controls">
                                        	<ul name="influencer[]" id="demo4"></ul>
                                            <small>Hit 'Enter/Tab/Comma' after write an Influencer Name.</small>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                    	<button type="submit" class="btn btn-primary" name="save">Save</button> 
                                        <button type="reset" class="btn">Cancel</button>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include(dirname(__FILE__) . '/template/extra.php'); ?>

<?php include(dirname(__FILE__) . '/template/footer.php'); ?>