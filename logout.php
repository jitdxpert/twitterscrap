<?php 
require_once(dirname(__FILE__) . '/config/config.php');
session_destroy();
header('location:' . SITE_URL);
exit;