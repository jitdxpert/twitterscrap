<?php
ini_set("display_errors", 1);
$server = $_SERVER['HTTP_HOST'];
switch ($server) {
	case 'localhost':
		$env = 'development';
		break;
	case '54.242.158.50':
		$env = 'testing';
		break;
	default:
		$env = 'production';
		break;
}
define('ENVIRONMENT', $env);
require_once(dirname(__FILE__) . '/' . ENVIRONMENT . "/declare.php");
require("twitter/twitteroauth.php");
date_default_timezone_set('UTC');
$scon = mysql_connect(DB_SERVER_NAME,DB_USER_NAME,DB_PASSWORD) or die('DB Error');
$sdb = mysql_select_db(DB_NAME);
session_start();
?>
