<?php
/*
	This file contains mongodb collections details
*/
$server = $_SERVER['HTTP_HOST'];
switch ($server) {
	case 'localhost':
		$env = 'development';
		break;
	default:
		$env = 'production';
		break;
}
define('ENVIRONMENT', $env);
require_once("../config/".ENVIRONMENT."/declare.php");
require_once("../config/mongodb.php");
$mongoDb->createCollection("influence_followers",array("autoIndexId"=>1));
$mongoDb->pages->ensureIndex(array("followers_id"=>1),array("unique"=>1));



?>