<div class="footer">
    <div class="footer-inner">
        <div class="container">
            <div class="row">
                <div class="span12"> &copy; 2016 <a target="_blank" href="<?php echo SITE_URL; ?>">EngageWise</a>.</div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo SITE_URL; ?>js/bootstrap.js"></script>

<!-- login page -->
<script src="<?php echo SITE_URL; ?>js/signin.js"></script>

<!-- dashboard page -->
<script src="<?php echo SITE_URL; ?>js/excanvas.min.js"></script>
<script src="<?php echo SITE_URL; ?>js/chart.min.js" type="text/javascript"></script>
<script src="<?php echo SITE_URL; ?>js/full-calendar/fullcalendar.min.js"></script>
<script src="<?php echo SITE_URL; ?>js/base.js"></script>

</body>
</html>