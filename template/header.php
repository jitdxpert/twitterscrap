<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    
    <title><?php echo ucwords($page_name); ?> - Twitter Scrap</title>
    
	<link href="<?php echo SITE_URL; ?>css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo SITE_URL; ?>css/bootstrap-responsive.min.css" rel="stylesheet">
    
	<link href="<?php echo SITE_URL; ?>css/font-awesome.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
	
	<link href="<?php echo SITE_URL; ?>css/style.css" rel="stylesheet">
    
    <link href="<?php echo SITE_URL; ?>css/pages/signin.css" rel="stylesheet">
	<link href="<?php echo SITE_URL; ?>css/pages/dashboard.css" rel="stylesheet">
    <link href="<?php echo SITE_URL; ?>css/pages/reports.css" rel="stylesheet">
    <link href="<?php echo SITE_URL; ?>css/pages/plans.css" rel="stylesheet">
    <link href="<?php echo SITE_URL; ?>css/pages/faq.css" rel="stylesheet">
    
    <link href="<?php echo SITE_URL; ?>css/tagit-stylish-yellow.css" rel="stylesheet" type="text/css" />
    
    <script src="<?php echo SITE_URL; ?>js/jquery-1.7.2.min.js"></script>
    <script src="<?php echo SITE_URL; ?>js/jquery-ui.1.8.20.min.js"></script>
    <script src="<?php echo SITE_URL; ?>js/tagit.js"></script>
	<script src="<?php echo SITE_URL; ?>js/scripts.js" type="text/javascript"></script>
    
    <!--[if lt IE 9]>
	    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>