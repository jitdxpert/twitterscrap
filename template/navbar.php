<div class="subnavbar">
    <div class="subnavbar-inner">
        <div class="container">
            <ul class="mainnav">
                <li>
                    <a href="#"><i class="icon-dashboard"></i><span>Dashboard</span></a>
                </li>
                <li<?php echo $page_name=='category'?' class="active"':''; ?>>
                    <a href="<?php echo SITE_URL; ?>category/"><i class="icon-list-alt"></i><span>Category</span></a>
                </li>
                <li>
                    <a href="#"><i class="icon-facetime-video"></i><span>Menu Item 1</span></a>
                </li>
                <li>
                    <a href="#"><i class="icon-bar-chart"></i><span>Menu Item 2</span></a>
                </li>
                <li>
                    <a href="#"><i class="icon-code"></i><span>Menu Item 3</span></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-long-arrow-down"></i><span>Menu Item 4</span> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Sub Menu Item 1</a></li>
                        <li><a href="#">Sub Menu Item 2</a></li>
                        <li><a href="#">Sub Menu Item 3</a></li>
                        <li><a href="#">Sub Menu Item 4</a></li>
                        <li><a href="#">Sub Menu Item 5</a></li>
                        <li><a href="#">Sub Menu Item 6</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>